![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)

# M2 - Ejercicio Jobs

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| Xavier Bonet Daga | Master | Project Manager | 01/02/2021 |   |   |  |
| David Bonet Daga | Master | Team Member | 01/02/2021 |   |   |  |

#### 2. Description
```
Ejercicio Jobs ( Cada rama MILESTONE 1/2/3 es una de las fases del ejercicio, la 3 es la version final)
```

#### 3. Link a un demo con el proyecto desplegado: https://gitlab.com/xavi_bonet/m2.git

```
M2 - Ejercicio Jobs / https://gitlab.com/xavi_bonet/m2.git
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```
Java - jdk-8
IDE - Eclipse Enterprise 
```
###### Command line 
```

```

#### 5. Screenshot imagen que indique cómo debe verse el proyecto.
